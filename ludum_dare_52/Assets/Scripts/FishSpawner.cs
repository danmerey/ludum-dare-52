using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class FishSpawner : MonoBehaviour
{
    public const float SPAWNER_WORK_DISTANCE = 50f; // what distance player needs to be so this spawner work
    
    public const float SPAWN_MIN_COOLDOWN = 0.75f;
    public const float SPAWN_MAX_COOLDOWN = 2f;

    private const int SPAWN_FISHES_ON_START = 75;
    
    private const float SPAWN_CHANCE_GIANT_GOLDFISH = 0.0003f;
    private const float SPAWN_CHANCE_CAGEY_CATFISH = 0.002f;
    private const float SPAWN_CHANCE_SAGE_SALMON = 0.002f;
    private const float SPAWN_CHANCE_SHY_SHARK = 0.002f;
    private const float SPAWN_CHANCE_SNAPPY_SNAPPER = 0.002f;
    private const float SPAWN_CHANCE_COOL_COD = 0.3f;
    private const float SPAWN_CHANCE_FISHY_FISH = 1f;
    
    private const int FISH_MIN_SPAWN_DISTANCE_FROM_SPAWNER = 2;
    private const int FISH_MAX_SPAWN_DISTANCE_FROM_SPAWNER = 9;

    private float SpawnCooldown;
    
    private Transform _transform;

    private void Awake()
    {
        _transform = GetComponent<Transform>();
    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < SPAWN_FISHES_ON_START; i++)
        {
            SpawnFish(true);   
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerIsCloseEnough())
        {
            // wind angle will change when the cooldown is over
            SpawnCooldown -= Time.fixedDeltaTime;
            if (SpawnCooldown <= 0)
            {
                SpawnFish(false);
                SpawnCooldown += Random.Range(SPAWN_MIN_COOLDOWN, SPAWN_MAX_COOLDOWN);
            }
        }
    }

    bool PlayerIsCloseEnough()
    {
        return (Vector2.Distance(Main.PlayerShip._transform.position, _transform.position) <= SPAWNER_WORK_DISTANCE);
    }

    void SpawnFish(bool randomLifePercentage)
    {
        float randomRadius = Random.Range(FISH_MIN_SPAWN_DISTANCE_FROM_SPAWNER, FISH_MAX_SPAWN_DISTANCE_FROM_SPAWNER);
        float randomAngleRad = Random.Range(0, Mathf.PI * 2);

        Fish.FishTypes randomFishTypes = Fish.FishTypes.FishyFish;

        if (Random.Range(0, 1f) <= SPAWN_CHANCE_GIANT_GOLDFISH) randomFishTypes = Fish.FishTypes.GiantGoldfish;
        else if (Random.Range(0, 1f) <= SPAWN_CHANCE_CAGEY_CATFISH) randomFishTypes = Fish.FishTypes.CageyCatfish;
        else if (Random.Range(0, 1f) <= SPAWN_CHANCE_SAGE_SALMON) randomFishTypes = Fish.FishTypes.SageSalmon;
        else if (Random.Range(0, 1f) <= SPAWN_CHANCE_SHY_SHARK) randomFishTypes = Fish.FishTypes.ShyShark;
        else if (Random.Range(0, 1f) <= SPAWN_CHANCE_SNAPPY_SNAPPER) randomFishTypes = Fish.FishTypes.SnappySnapper;
        else if (Random.Range(0, 1f) <= SPAWN_CHANCE_COOL_COD) randomFishTypes = Fish.FishTypes.CoolCod;
        
        GameObject newFish = Instantiate(Resources.Load("Fish/Fish" + randomFishTypes) as GameObject);
        newFish.transform.eulerAngles = new Vector3(0, 0, Random.Range(0, 360f));

        newFish.transform.position = _transform.position + new Vector3(Mathf.Cos(randomAngleRad), Mathf.Sin(randomAngleRad), 0) * randomRadius;

        if (randomLifePercentage)
        {
            newFish.GetComponent<Fish>().SetRandomLife(); // doesn't work yet, probably because of the execution order
        }
    }
}
