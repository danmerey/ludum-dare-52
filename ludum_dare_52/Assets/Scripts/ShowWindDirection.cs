using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowWindDirection : MonoBehaviour
{
    private Transform _transform;
    
    void Awake()
    {
        _transform = transform;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _transform.eulerAngles = new Vector3(0, 0, Main.WindAngleDegrees);
    }
}
