using System;
using TMPro;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;

public class Ship : MonoBehaviour
{
    private enum HookStates
    {
        OnCooldown, // not used
        Ready,
        Flying,
        UnderWater
    }

    private const float HOOK_HARVEST_DISTANCE = 0.2f;
    private const float HOOK_THROW_MAX_RANGE = 3.5f;
    private const float HOOK_PULL_SPEED = 70f;
    private const float HOOK_FORCED_AUTOPULL_DISTANCE = 4f;
    private const float HOOK_FORCED_AUTOPULL_SPEED = 17.5f;
    private const float HOOK_FLYING_TIME_MULTIPLIER = 0.55f;
    
    private const float SHIP_MOVE_SPEED = 10f;
    private const float SHIP_MOVE_SPEED_PER_UPGRADE = 1f;
    private const float SHIP_TURN_SPEED = 0.03f;
    private const float SHIP_TURN_WHILE_NOT_MOVING = 0.5f;
    private const float SHIP_TURN_WHILE_NOT_MOVING_PER_UPGRADE = 0.05f;
    
    private const float ROWING_POWER = 1f;
    private const float ROWING_POWER_PER_UPGRADE = 0.5f;
    
    private const float POINT_OF_SAIL_ANGLE_CLOSE_HAULED = 85f;
    private const float POINT_OF_SAIL_ANGLE_BEAM_REACH = 30f;
    private const float POINT_OF_SAIL_ANGLE_BROAD_REACH_AND_RUNNING = 20f;
    private const float POINT_OF_SAIL_POWER_INTO_THE_WIND = 0f;
    private const float POINT_OF_SAIL_POWER_CLOSE_HAULED = 0.25f;
    private const float POINT_OF_SAIL_POWER_BEAM_REACH = 0.9f;
    private const float POINT_OF_SAIL_POWER_BROAD_REACH_AND_RUNNING = 1f;
    
    private const float POINT_OF_SAIL_NOT_ALLOWED_ANGLE = 110f;
    
    private const float SAILS_SET_MIN = 0f;
    private const float SAILS_SET_MAX = 1f;
    private const float SET_SAILS_SPEED = 1f;
    
    private const float STEERING_WHEEL_ANGLE_MAX = 30f;
    private const float STEERING_WHEEL_ANGLE_MIN = -STEERING_WHEEL_ANGLE_MAX;
    private const float STEERING_WHEEL_CHANGE_SPEED = 60f;
    private const float STEERING_WHEEL_RETURNING_TO_ZERO_SPEED = 30f;

    private float SailsSet; // from 0 to 1 (how sails are set by captain)
    private float SteeringWheelAngleDegrees;

    private HookStates CurrentHookState;

    public Sprite[] SailsSprites;
    public Rigidbody2D _rigidbody2d;
    public Transform _transform;
    public Transform _rotatable;
    public SpriteRenderer _sailsSpriteRenderer;

    private float HookCurrentFlyingTime;
    private float HookFlyingTimeMax;
    private Vector3 HookFlyingStartPosition;
    private Vector3 HookFlyingEndPosition;
    private Transform _hookTransform;
    private Transform _hookRopeTransform;
    private Transform _hookMovableTransform;
    private Transform _hookCaughtFishTransform;
    private Transform _hookMoveByZTransform;
    private GameObject _hookMovableGameObject;
    private Rigidbody2D _hookMovableRigidbody;
    private Transform _hookPartsTransform;
    private bool DontLetUseHookUntilPlayerStopsPressingButton;
    private CircleCollider2D _hookCollider;
    
    private GameObject _hookTopSpriteGameObject;

    private bool SailsRotationModeIsActive;
    
    void Awake()
    {
        _rigidbody2d = GetComponent<Rigidbody2D>();
        _transform = GetComponent<Transform>();
        _rotatable = _transform.Find("SailsMain/Rotatable");
        _sailsSpriteRenderer = _rotatable.Find("Sails").GetComponent<SpriteRenderer>();
        
        _hookPartsTransform = _transform.Find("HookParts");
        _hookMovableTransform = _hookPartsTransform.Find("Movable");
        _hookCaughtFishTransform = _hookMovableTransform.Find("CaughtFish");
        _hookMoveByZTransform = _hookMovableTransform.Find("MoveByZ");
        _hookMovableGameObject = _hookMovableTransform.gameObject; 
        _hookMovableRigidbody = _hookMovableTransform.GetComponent<Rigidbody2D>();
        _hookMovableTransform.parent = null;
        
        _hookTransform = _hookMoveByZTransform.Find("Hook");
        _hookCollider = _hookMovableTransform.GetComponent<CircleCollider2D>();
        _hookTopSpriteGameObject = _hookTransform.Find("TopSprite").gameObject;
        _hookRopeTransform = _hookMoveByZTransform.Find("HookRope");
    }

    private void Update()
    {
        if ((DontLetUseHookUntilPlayerStopsPressingButton) && (Main.LeftMouseButtonPressedLastFrame) && (!Main.LeftMouseButtonPressed))
        {
            DontLetUseHookUntilPlayerStopsPressingButton = false; // now player can use hook again
        }   
    }

    void Start()
    {
        ResetHook(true);
    }

    void FixedUpdate()
    {
        float rowingPower = 0;
        
        // if this ship is controlled by player
        if (Main.PlayerShip == this)
        {
            ControlShipSteeringWheel(Main.ShipMovementSteeringWheel);
            ControlShipSails(Main.ShipMovementSails);
            rowingPower = (ROWING_POWER + ROWING_POWER_PER_UPGRADE * Main.CurrentUpgradeLevel) * Main.ShipMovementSails;
            
            if (Main.LeftMouseButtonPressed) UseHook();
            AutoPullHookIfNeeded();
        }
        
        RotateSails();
        
        // steering wheel is returning to center
        if (Main.ShipMovementSteeringWheel == 0)
        {
            SteeringWheelAngleDegrees = Mathf.MoveTowards(SteeringWheelAngleDegrees, 0, STEERING_WHEEL_RETURNING_TO_ZERO_SPEED * Time.fixedDeltaTime);
        }
        
        // rotate ship
        {
            _rigidbody2d.AddTorque(-SHIP_TURN_SPEED * (SHIP_TURN_WHILE_NOT_MOVING + SHIP_TURN_WHILE_NOT_MOVING_PER_UPGRADE * Main.CurrentUpgradeLevel + _rigidbody2d.velocity.magnitude) * SteeringWheelAngleDegrees);
        }
        
        // move ship
        {
            float sailsAngleDeg = _rotatable.eulerAngles.z;
            float shipAngleDeg = _transform.eulerAngles.z;
            float shipAngleRad = shipAngleDeg * Mathf.Deg2Rad;
            float deltaAngleSailsAndWindDeg = Mathf.DeltaAngle(Main.WindAngleDegrees, sailsAngleDeg);
            float windSailingPower = GetWindSailingPowerFromPointOfSail(deltaAngleSailsAndWindDeg); // does the wind blow in the sails enough?
            float deltaAngleShipAndSailsDeg = Mathf.DeltaAngle(sailsAngleDeg, shipAngleDeg);
            float deltaAngleShipAndSailsRad = deltaAngleShipAndSailsDeg * Mathf.Deg2Rad;
            float sailsSailingPower = Mathf.Clamp(Mathf.Cos(deltaAngleShipAndSailsRad), 0, 1); // is angle facing the nose of the boat enough?

            float finalSailingPower = SailsSet * windSailingPower;
            SetSailsSprite(finalSailingPower);

            Vector2 addForce = new Vector2(Mathf.Cos(shipAngleRad), Mathf.Sin(shipAngleRad)) * (finalSailingPower * sailsSailingPower * (SHIP_MOVE_SPEED + SHIP_MOVE_SPEED_PER_UPGRADE * Main.CurrentUpgradeLevel) + rowingPower);
            _rigidbody2d.AddForce(addForce, ForceMode2D.Force);
        }
        
        HookFlying();
        RotateHook();
        StretchRope();
    }

    float GetWindSailingPowerFromPointOfSail(float angleDifferenceDeg)
    {
        float asbAngleDifferenceDeg = Mathf.Abs(angleDifferenceDeg);

        if (asbAngleDifferenceDeg <= POINT_OF_SAIL_ANGLE_BROAD_REACH_AND_RUNNING) return POINT_OF_SAIL_POWER_BROAD_REACH_AND_RUNNING;
        
        if (asbAngleDifferenceDeg <= POINT_OF_SAIL_ANGLE_BEAM_REACH) return Mathf.Lerp(POINT_OF_SAIL_POWER_BROAD_REACH_AND_RUNNING, POINT_OF_SAIL_POWER_BEAM_REACH,
            (asbAngleDifferenceDeg - POINT_OF_SAIL_ANGLE_BROAD_REACH_AND_RUNNING) / (POINT_OF_SAIL_ANGLE_BEAM_REACH - POINT_OF_SAIL_ANGLE_BROAD_REACH_AND_RUNNING));
        
        if (asbAngleDifferenceDeg <= POINT_OF_SAIL_ANGLE_CLOSE_HAULED) return Mathf.Lerp(POINT_OF_SAIL_POWER_BEAM_REACH, POINT_OF_SAIL_POWER_CLOSE_HAULED,
            (asbAngleDifferenceDeg - POINT_OF_SAIL_ANGLE_BEAM_REACH) / (POINT_OF_SAIL_ANGLE_CLOSE_HAULED - POINT_OF_SAIL_ANGLE_BEAM_REACH));

        return POINT_OF_SAIL_POWER_INTO_THE_WIND;
    }

    void RotateSails()
    {
        if (SailsRotationModeIsActive) // rotate to mouse
        {
            float newAngleDeg = (Mathf.Atan2(Main.WorldMousePosition.y - _rotatable.position.y, Main.WorldMousePosition.x - _rotatable.position.x)) * Mathf.Rad2Deg;

            // instant rotation
            _rotatable.eulerAngles = new Vector3(0, 0, newAngleDeg);

            float newLocalAngleDeg = AngleMath.NormalizeAngleDegrees(_rotatable.localEulerAngles.z);
            
            // protections from wrong angles
            if ((newLocalAngleDeg > 180 - POINT_OF_SAIL_NOT_ALLOWED_ANGLE) && (newLocalAngleDeg <= 180)) _rotatable.localEulerAngles = new Vector3(0, 0, 180 - POINT_OF_SAIL_NOT_ALLOWED_ANGLE);
            else if ((newLocalAngleDeg > 180) && (newLocalAngleDeg <= 180 + POINT_OF_SAIL_NOT_ALLOWED_ANGLE)) _rotatable.localEulerAngles = new Vector3(0, 0, 180 + POINT_OF_SAIL_NOT_ALLOWED_ANGLE);
        }
    }

    private void ControlShipSteeringWheel(int direction)
    {
        SteeringWheelAngleDegrees += direction * STEERING_WHEEL_CHANGE_SPEED * Time.fixedDeltaTime;
        SteeringWheelAngleDegrees = Mathf.Clamp(SteeringWheelAngleDegrees, STEERING_WHEEL_ANGLE_MIN, STEERING_WHEEL_ANGLE_MAX);
    }
    
    private void ControlShipSails(int direction)
    {
        SailsSet += direction * SET_SAILS_SPEED * Time.fixedDeltaTime;
        SailsSet = Mathf.Clamp(SailsSet, SAILS_SET_MIN, SAILS_SET_MAX);
    }
    
    public void SetSailsRotationModeActive(bool active)
    {
        if (active) // enable
        {
            SailsRotationModeIsActive = true;
        }
        else // disable
        {
            SailsRotationModeIsActive = false;
        }
    }
    
    private void SetSailsSprite(float sailingPower)
    {
        int number = Mathf.Clamp((int)(sailingPower * SailsSprites.Length) - 1, 0, SailsSprites.Length - 1);
        _sailsSpriteRenderer.sprite = SailsSprites[number];
    }

    public void UseHook()
    {
        if (!DontLetUseHookUntilPlayerStopsPressingButton)
        {
            if (CurrentHookState == HookStates.Ready)
            {
                ThrowHook();
            }
        }
        
        if (CurrentHookState == HookStates.UnderWater) // pulling hook
        {
            PullHook();
        }
        else // can't control hook right now
        {
                
        }
    }

    private void ThrowHook()
    {
        _hookMovableGameObject.SetActive(true);
        
        HookFlyingStartPosition = _hookPartsTransform.position;
        HookFlyingEndPosition  = Main.WorldMousePosition;
        float distanceBetweenStartAndEnd = Vector3.Distance(HookFlyingStartPosition, HookFlyingEndPosition);
        
        // hook can't be thrown too far 
        if (distanceBetweenStartAndEnd > HOOK_THROW_MAX_RANGE)
        {
            float angleRad = AngleMath.AngleBetween2dPointsRad(HookFlyingStartPosition, HookFlyingEndPosition);
            HookFlyingEndPosition = HookFlyingStartPosition + new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad)) * HOOK_THROW_MAX_RANGE;
        }

        

        //
        
        float distance = Vector3.Distance(HookFlyingStartPosition, HookFlyingEndPosition);
        HookFlyingTimeMax = Mathf.Pow(distance, 0.25f) * HOOK_FLYING_TIME_MULTIPLIER;
        HookCurrentFlyingTime = HookFlyingTimeMax;
        _hookTopSpriteGameObject.SetActive(true);
                
        DontLetUseHookUntilPlayerStopsPressingButton = true; // prevent accidental hook throwing
        CurrentHookState = HookStates.Flying;
    }

    private void PullHook()
    {
        float faceAngleRad = AngleMath.AngleBetween2dPointsRad(_hookMovableTransform.position, Main.PlayerShip._transform.position);
        _hookMovableRigidbody.AddForce(new Vector2(Mathf.Cos(faceAngleRad), Mathf.Sin(faceAngleRad)) * (HOOK_PULL_SPEED), ForceMode2D.Force);
        
        float distance = Vector3.Distance(_hookMovableRigidbody.position, _hookPartsTransform.position);

        if (distance <= HOOK_HARVEST_DISTANCE)
        {
            CollectAllHookedFish();
            ResetHook(false);
        }
    }
    
    private void AutoPullHookIfNeeded()
    {
        if (CurrentHookState == HookStates.UnderWater)
        {
            float faceAngleRad = AngleMath.AngleBetween2dPointsRad(_hookMovableTransform.position, Main.PlayerShip._transform.position);
            float distance = Vector3.Distance(_hookMovableRigidbody.position, _hookPartsTransform.position);
            if (distance >= HOOK_FORCED_AUTOPULL_DISTANCE)
            {
                _hookMovableRigidbody.AddForce(new Vector2(Mathf.Cos(faceAngleRad), Mathf.Sin(faceAngleRad)) * (HOOK_FORCED_AUTOPULL_SPEED * distance), ForceMode2D.Force);
            }
        }
    }
    
    private void ResetHook(bool startGame)
    {
        _hookMovableGameObject.SetActive(false);
        _hookCollider.enabled = false;
        
        CurrentHookState = HookStates.Ready;
        
        if (!startGame) DontLetUseHookUntilPlayerStopsPressingButton = true;
    }
    
    private void PutHookUnderWater()
    {
        HookCurrentFlyingTime = 0;
        ChangeHookPosition(HookFlyingEndPosition);
        
        _hookTopSpriteGameObject.SetActive(false);
        _hookCollider.enabled = true;

        CurrentHookState = HookStates.UnderWater;
    }

    private void HookFlying()
    {
        if (CurrentHookState == HookStates.Flying)
        {
            HookCurrentFlyingTime -= Time.fixedDeltaTime;
            if (HookCurrentFlyingTime <= 0)
            {
                PutHookUnderWater();
                HookCurrentFlyingTime = 0;
            }
            
            // move hook
            {
                float t = 1 - (HookCurrentFlyingTime / HookFlyingTimeMax);
                _hookMovableTransform.position = Vector3.Lerp(HookFlyingStartPosition, HookFlyingEndPosition, t);

                float a = ((t * 2f) - 1f) * Mathf.PI * 0.5f;
                float h = Mathf.Cos(a);

                _hookMoveByZTransform.localPosition = new Vector3(0, 0, -h * 0.6f); 
                
                _hookRopeTransform.LookAt(_hookPartsTransform.position);
            }
        }
    }

    private void ChangeHookPosition(Vector3 position)
    {
        _hookMovableTransform.position = position;
    }
    
    private void RotateHook()
    {
        float newAngleDeg = AngleMath.AngleBetween2dPointsDeg(_hookPartsTransform.position,_hookMovableTransform.position);
        _hookMovableTransform.eulerAngles = new Vector3(0, 0, newAngleDeg);
    }
    
    private void StretchRope()
    {
        const float TEXTURE_WIDTH = 200;
        const float PIXELS_PER_UNIT = 100;
        float distance = Vector3.Distance(_hookMovableTransform.position, _hookPartsTransform.position);
        
        _hookRopeTransform.localScale = new Vector3(1, 1, distance * TEXTURE_WIDTH / PIXELS_PER_UNIT / 2f);
    }

    private void CollectAllHookedFish()
    {
        bool numberChanged = false;
        
        
        foreach(Transform child in _hookCaughtFishTransform)
        {
            switch (child.GetComponent<Fish>().FishType)
            {
                case Fish.FishTypes.FishyFish:
                {
                    Main.ResourceFish += 1;
                    numberChanged = true;
                    if (!Main.Quest1Completed)
                    {
                        Main.Quest1Completed = true;
                        GameObject.Find("CanvasGUI/Quest1").SetActive(false);
                        Main.Quest1CompletedObject.SetActive(true);
                        CheckForCompletingAllQuestsForTheFirstTime();
                    }
                    break;
                }
                case Fish.FishTypes.CoolCod:
                {
                    Main.ResourceFish += 3;
                    numberChanged = true;
                    if (!Main.Quest2Completed)
                    {
                        Main.Quest2Completed = true;
                        GameObject.Find("CanvasGUI/Quest2").SetActive(false);
                        Main.Quest2CompletedObject.SetActive(true);
                        CheckForCompletingAllQuestsForTheFirstTime();
                    }
                    break;
                }
                case Fish.FishTypes.SnappySnapper:
                {
                    Main.ResourceFish += 10;
                    numberChanged = true;
                    if (!Main.Quest3Completed)
                    {
                        Main.Quest3Completed = true;
                        GameObject.Find("CanvasGUI/Quest3").SetActive(false);
                        Main.Quest3CompletedObject.SetActive(true);
                        CheckForCompletingAllQuestsForTheFirstTime();
                    }
                    break;
                }
                case Fish.FishTypes.ShyShark:
                {
                    Main.ResourceFish += 20;
                    numberChanged = true;
                    if (!Main.Quest4Completed)
                    {
                        Main.Quest4Completed = true;
                        GameObject.Find("CanvasGUI/Quest4").SetActive(false);
                        Main.Quest4CompletedObject.SetActive(true);
                        CheckForCompletingAllQuestsForTheFirstTime();
                    }
                    break;
                }
                case Fish.FishTypes.SageSalmon:
                {
                    Main.ResourceFish += 20;
                    numberChanged = true;
                    if (!Main.Quest5Completed)
                    {
                        Main.Quest5Completed = true;
                        GameObject.Find("CanvasGUI/Quest5").SetActive(false);
                        Main.Quest5CompletedObject.SetActive(true);
                        CheckForCompletingAllQuestsForTheFirstTime();
                    }
                    break;
                }
                case Fish.FishTypes.CageyCatfish:
                {
                    Main.ResourceFish += 30;
                    numberChanged = true;
                    if (!Main.Quest6Completed)
                    {
                        Main.Quest6Completed = true;
                        GameObject.Find("CanvasGUI/Quest6").SetActive(false);
                        Main.Quest6CompletedObject.SetActive(true);
                        CheckForCompletingAllQuestsForTheFirstTime();
                    }
                    break;
                }
                case Fish.FishTypes.GiantGoldfish:
                {
                    Main.ResourceFish += 100;
                    numberChanged = true;
                    if (!Main.Quest7Completed)
                    {
                        Main.Quest7Completed = true;
                        GameObject.Find("CanvasGUI/Quest7").SetActive(false);
                        Main.Quest7CompletedObject.SetActive(true);
                        CheckForCompletingAllQuestsForTheFirstTime();
                    }
                    break;
                }
            }
        }
        
        while (_hookCaughtFishTransform.childCount > 0)
        {
            DestroyImmediate(_hookCaughtFishTransform.GetChild(0).gameObject);
        }
        
        Main.UpdateResourceFishNumberInGUI();
        
        while (Main.ResourceFish >= Main.NeededFishForNextUpgrade)
        {
            Main.CurrentUpgradeLevel++;
            if (Main.CurrentUpgradeLevel == 1) Main.NeededFishForNextUpgrade = Main.AMOUNT_OF_FISH_NEEDED_FOR_UPGRADE_2;
            else if (Main.CurrentUpgradeLevel == 2) Main.NeededFishForNextUpgrade = Main.AMOUNT_OF_FISH_NEEDED_FOR_UPGRADE_3;
            else if (Main.CurrentUpgradeLevel == 3) Main.NeededFishForNextUpgrade = Main.AMOUNT_OF_FISH_NEEDED_FOR_UPGRADE_4;
            else if (Main.CurrentUpgradeLevel == 4) Main.NeededFishForNextUpgrade = Main.AMOUNT_OF_FISH_NEEDED_FOR_UPGRADE_5;
            else Main.NeededFishForNextUpgrade += Main.MOW_MUCH_MORE_FISH_NEEDED_FOR_EVERY_NEXT_UPGRADE;
            
            Main.UpdateNextUpgradeInGUI();
        }
    }

    void CheckForCompletingAllQuestsForTheFirstTime()
    {
        if (!Main.AllQuestsCompleted)
        {
            if ((Main.Quest1Completed) &&
                (Main.Quest2Completed) &&
                (Main.Quest3Completed) &&
                (Main.Quest4Completed) &&
                (Main.Quest5Completed) &&
                (Main.Quest6Completed) &&
                (Main.Quest7Completed))
            {
                Main.AllQuestsCompleted = true;
                GameObject.Find("CanvasGUI/QuestTitle").SetActive(false);
                Main.QuestTitleCompletedObject.SetActive(true);
            }
        }
    }
}
