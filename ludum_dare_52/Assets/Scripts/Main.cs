﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class Main : MonoBehaviour
{
    public const float WIND_STARTING_ANGLE = 45f;
    public const float WIND_CHANGE_ANGLE_MIN_COOLDOWN = 30f;
    public const float WIND_CHANGE_ANGLE_MAX_COOLDOWN = 60f;
    public const float WIND_CHANGE_ANGLE_SPEED = 3f;
    public const float WIND_CHANGE_ANGLE_MAX_RANDOM = 180f;
    
    public const float WIND_STRENGTH_MAX = 1f; // no other wind strength for now, only angle
    
    public static Ship PlayerShip;

    private float changeWindAngleCooldown;
    
    public static float WindAngleAimDegrees;
    public static float WindAngleDegrees = WindAngleAimDegrees;
    public static float WindStrength = WIND_STRENGTH_MAX;
    
    private Transform _transform;
    private Rigidbody2D _rigidbody;
    
    public static int ShipMovementSteeringWheel;
    public static int ShipMovementSails;
    public static float CameraZoomSpeed;
    public static bool ShiftHold;
    
    public static bool RightMouseButtonPressed;
    public static bool LeftMouseButtonPressed;
    public static bool MiddleMouseButtonPressed;
    
    public static bool RightMouseButtonPressedLastFrame;
    public static bool LeftMouseButtonPressedLastFrame;
    public static bool MiddleMouseButtonPressedLastFrame;

    public static bool SettingsSoundsMuted;
    public static bool SettingsMusicMuted;
    public static float SettingsSoundsVolume;
    public static float SettingsMusicVolume;
    
    public static TextMeshProUGUI ResourceFishNumberTextMesh;
    public static TextMeshProUGUI NextUpgradeTextMesh;

    public static Vector2 WorldMousePosition;

    
    
    public static bool Quest1Completed;
    public static bool Quest2Completed;
    public static bool Quest3Completed;
    public static bool Quest4Completed;
    public static bool Quest5Completed;
    public static bool Quest6Completed;
    public static bool Quest7Completed;
    public static bool AllQuestsCompleted;

    public static GameObject QuestTitleCompletedObject;
    public static GameObject Quest1CompletedObject;
    public static GameObject Quest2CompletedObject;
    public static GameObject Quest3CompletedObject;
    public static GameObject Quest4CompletedObject;
    public static GameObject Quest5CompletedObject;
    public static GameObject Quest6CompletedObject;
    public static GameObject Quest7CompletedObject;

    public const int AMOUNT_OF_FISH_NEEDED_FOR_UPGRADE_1 = 50;
    public const int AMOUNT_OF_FISH_NEEDED_FOR_UPGRADE_2 = 100;
    public const int AMOUNT_OF_FISH_NEEDED_FOR_UPGRADE_3 = 200;
    public const int AMOUNT_OF_FISH_NEEDED_FOR_UPGRADE_4 = 500;
    public const int AMOUNT_OF_FISH_NEEDED_FOR_UPGRADE_5 = 1000;
    public const int MOW_MUCH_MORE_FISH_NEEDED_FOR_EVERY_NEXT_UPGRADE = 1000;

    public static int ResourceFish; // currency
    public static int NeededFishForNextUpgrade = AMOUNT_OF_FISH_NEEDED_FOR_UPGRADE_1;
    public static int CurrentUpgradeLevel;
    
    private void Awake()
    {
        GenerateMap();
        
        _transform = GetComponent<Transform>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void ResetSettings()
    {
        SettingsSoundsMuted = false;
        SettingsMusicMuted = false;
        SettingsSoundsVolume = 1f;
        SettingsMusicVolume = 1f;
        Sounds.ChangeVolume();
    }
    
    void GenerateMap()
    {
        changeWindAngleCooldown = WIND_CHANGE_ANGLE_MIN_COOLDOWN;
        WindAngleDegrees = WIND_STARTING_ANGLE;
        
        // create ship for player
        GameObject playerShip = Instantiate(Resources.Load("Ships/ShipCommon") as GameObject);
        PlayerShip = playerShip.GetComponent<Ship>();
        GameObject.Find("CameraAim").transform.SetParent(playerShip.transform);

        // create wind center and wind particles
        GameObject windCenter = Instantiate(Resources.Load("Wind/WindCenter") as GameObject);
        for (int j = 0; j < Wind.WIND_PARTICLES_NUMBER; j++)
        {
            GameObject windParticle = Instantiate(Resources.Load("Wind/WindParticle") as GameObject);
            windParticle.transform.SetParent(windCenter.transform);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ResourceFishNumberTextMesh = GameObject.Find("CanvasGUI/ResourceFishNumber").GetComponent<TextMeshProUGUI>();
        NextUpgradeTextMesh = GameObject.Find("CanvasGUI/NextUpgrade").GetComponent<TextMeshProUGUI>();

        NeededFishForNextUpgrade = AMOUNT_OF_FISH_NEEDED_FOR_UPGRADE_1;
        CurrentUpgradeLevel = 0;
        ResourceFish = 0;
        UpdateResourceFishNumberInGUI();
        UpdateNextUpgradeInGUI();

        Quest1Completed = false;
        Quest2Completed = false;
        Quest3Completed = false;
        Quest4Completed = false;
        Quest5Completed = false;
        Quest6Completed = false;
        Quest7Completed = false;
        AllQuestsCompleted = false;

        QuestTitleCompletedObject = GameObject.Find("CanvasGUI/QuestTitleDone");
        Quest1CompletedObject = GameObject.Find("CanvasGUI/QuestDone1");
        Quest2CompletedObject = GameObject.Find("CanvasGUI/QuestDone2");
        Quest3CompletedObject = GameObject.Find("CanvasGUI/QuestDone3");
        Quest4CompletedObject = GameObject.Find("CanvasGUI/QuestDone4");
        Quest5CompletedObject = GameObject.Find("CanvasGUI/QuestDone5");
        Quest6CompletedObject = GameObject.Find("CanvasGUI/QuestDone6");
        Quest7CompletedObject = GameObject.Find("CanvasGUI/QuestDone7");

        QuestTitleCompletedObject.SetActive(false);
        Quest1CompletedObject.SetActive(false);
        Quest2CompletedObject.SetActive(false);
        Quest3CompletedObject.SetActive(false);
        Quest4CompletedObject.SetActive(false);
        Quest5CompletedObject.SetActive(false);
        Quest6CompletedObject.SetActive(false);
        Quest7CompletedObject.SetActive(false);

        RightMouseButtonPressed = false;
        LeftMouseButtonPressed = false;
        MiddleMouseButtonPressed = false;

        RightMouseButtonPressedLastFrame = false;
        LeftMouseButtonPressedLastFrame = false;
        MiddleMouseButtonPressedLastFrame = false;
    }

    // Update is called once per frame
    void Update()
    {
        // reset variables
        {
        }

        // detect controls
        {
            DetectControls();
            DetectDebugControls();
        }
    }

    void FixedUpdate()
    {
        WorldMousePosition = GameCamera.MainGameCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -GameCamera.Zoom()));

        // wind angle will change when the cooldown is over
        changeWindAngleCooldown -= Time.fixedDeltaTime;
        if (changeWindAngleCooldown <= 0)
        {
            WindAngleAimDegrees = AngleMath.NormalizeAngleDegrees(WindAngleAimDegrees + Random.Range(-WIND_CHANGE_ANGLE_MAX_RANDOM, WIND_CHANGE_ANGLE_MAX_RANDOM)); 
            changeWindAngleCooldown += Random.Range(WIND_CHANGE_ANGLE_MIN_COOLDOWN, WIND_CHANGE_ANGLE_MAX_COOLDOWN);
        }
        
        // change angle of the wind if needed
        if (Mathf.Abs(Mathf.DeltaAngle(WindAngleAimDegrees, WindAngleDegrees)) >= 0.000001f)
        {
            WindAngleDegrees = Mathf.MoveTowardsAngle(WindAngleDegrees, WindAngleAimDegrees, Time.fixedDeltaTime * WIND_CHANGE_ANGLE_SPEED);
        }
    }

    private void DetectDebugControls()
    {
        // debug controls (only in editor)
        if (Platform.InEditor())
        {
            if (Input.GetKeyDown(KeyCode.Pause)) // pause editor
            {
                DebugGame.PauseEditor();
            }
            
            if (Input.GetKeyDown(KeyCode.V)) // toggle video screenshotting
            {
                DebugGame.ContinuousScreenshoting = !DebugGame.ContinuousScreenshoting;
                if (DebugGame.ContinuousScreenshoting) DebugGame.StartVideoScreenshoting();
                else DebugGame.FinishVideoScreenshoting();
            }

            if (Input.GetKeyDown(KeyCode.P)) // screenshot
            {
                DebugGame.MakeScreenshot();
            }
        }
    }
    
    private void DetectControls()
    {
        // ship movement
        if (PlayerShip != null)
        {
            ShipMovementSteeringWheel = 0;
            ShipMovementSails = 0;
            if ((Input.GetKey(KeyCode.A)) || (Input.GetKey(KeyCode.LeftArrow))) ShipMovementSteeringWheel--;
            if ((Input.GetKey(KeyCode.D)) || (Input.GetKey(KeyCode.RightArrow))) ShipMovementSteeringWheel++;
            if ((Input.GetKey(KeyCode.S)) || (Input.GetKey(KeyCode.DownArrow))) ShipMovementSails--;
            if ((Input.GetKey(KeyCode.W)) || (Input.GetKey(KeyCode.UpArrow))) ShipMovementSails++;
        }
        
        // modifiers
        {
            ShiftHold = ((Input.GetKey(KeyCode.LeftShift)) || (Input.GetKey(KeyCode.RightShift)));
        }

        // mouse
        {
            LeftMouseButtonPressedLastFrame = LeftMouseButtonPressed;
            LeftMouseButtonPressed = Input.GetKey(KeyCode.Mouse0);
            
            RightMouseButtonPressedLastFrame = RightMouseButtonPressed;
            RightMouseButtonPressed = Input.GetKey(KeyCode.Mouse1);

            MiddleMouseButtonPressedLastFrame = MiddleMouseButtonPressed;
            MiddleMouseButtonPressed = Input.GetKey(KeyCode.Mouse2);

            CameraZoomSpeed = Input.GetAxis("Mouse ScrollWheel");

            // control ship
            if (PlayerShip != null)
            {
                if ((RightMouseButtonPressed) && (!RightMouseButtonPressedLastFrame)) // just pressed RMB
                {
                    PlayerShip.SetSailsRotationModeActive(true);
                }
                else if ((!RightMouseButtonPressed) && (RightMouseButtonPressedLastFrame)) // just stopped pressing RMB
                {
                    PlayerShip.SetSailsRotationModeActive(false);
                }
            }
        }
    }

    public static void UpdateResourceFishNumberInGUI()
    {
        ResourceFishNumberTextMesh.text = "Fish caught: " + ResourceFish;
    }
    
    public static void UpdateNextUpgradeInGUI()
    {
        NextUpgradeTextMesh.text = "Next speed upgrade at: " + NeededFishForNextUpgrade;
    }
}
