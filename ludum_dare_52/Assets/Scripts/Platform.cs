using UnityEngine;


public class Platform
{
    public static bool SupportsComplicatedFonts() // fonts like Chinese, Japanese, Korean etc.
    {
        return !WebBuild(); // on webGL fallback fonts are not supported and fonts for these languages are too big
    }

    public static bool PcBuild() // is it build on PC (uses mouse and keyboard)
    {
        return (!MobileBuild() || InEditor());
    }

    public static bool WebBuild() // is it web build?
    {
        return (Application.platform == RuntimePlatform.WebGLPlayer);
    }

    public static bool MobileBuild() // is it mobile build?
    {
        return ((Application.platform == RuntimePlatform.Android) || (Application.platform == RuntimePlatform.IPhonePlayer));
    }

    public static bool InEditor() // in editor
    {
        if ((Application.isEditor) || (DebugGame.ConsiderPlatformAsEditor)) return true;

        return false;
    }

    public static bool InEditMode() // in editor (and not playing)
    {
        if ((Application.isEditor) && (!Application.isPlaying)) return true;

        return false;
    }

    public static bool IsAppleDevice()
    {
        return ((Application.platform == RuntimePlatform.IPhonePlayer) || (Application.platform == RuntimePlatform.tvOS) || (Application.platform == RuntimePlatform.OSXPlayer));
    }

    public static bool SupportsRestorePurchasesButton()
    {
        return (IsAppleDevice() || InEditor());
    }

    public static bool SupportsIapPurchases()
    {
        return ((Application.platform == RuntimePlatform.Android) || (Application.platform == RuntimePlatform.IPhonePlayer) || (InEditor()));
    }
}