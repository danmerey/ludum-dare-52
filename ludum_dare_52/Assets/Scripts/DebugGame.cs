﻿using System;
using System.Globalization;
using System.Linq;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class DebugGame : MonoBehaviour
{
    public static void OnUpdate() // some debug actions that must be shown every moment
    {
        // ShowText("Test text");
    }
    

    // In RELEASE version all constants must be FALSE (except ONLY IN EDITOR ones)

    // GAMEPLAY AND GRAPHICS
    public const bool DebugEndlessUpgrades = false;

    // INFO, LOGS, DEBUG BUTTONS
    public const bool DebugZonesEnabledOnStart = false;
        
    // INTERNET

    // SECURITY
    public static bool DontEncryptPlayerProgress = false; // will corrupt saves if toggled in wrong situation

    // OTHER

    // PLATFORM
    public const bool ConsiderPlatformAsEditor = false;


    // ---------------------------------------------------------------------------------------
        
    // TESTING CONSTANTS
    public const string ScreenshotsDirectory = "..\\Screenshots";
        
    // TESTING VARIABLES
    public static bool ContinuousScreenshoting = false; // screenshoting all the time (can be toggled on/off)
    public static long ContinuousScreenshotingFrameNumber = 0; // current screenshoting frame number
    public static string ContinuousScreenshotingKeyword = ""; // keyword so you know what video this is
    public static TextMeshPro RealTimeText;
    
        
    public static void ShowText(params object[] text)
    {
        if (RealTimeText != null)
        {
            RealTimeText.text = "";
            for (int j = 0; j < text.Length; j++)
            {
                if (j > 0) RealTimeText.text += "\n";
                var textPart = text[j];
                if (textPart != null) RealTimeText.text += textPart.ToString();
                else RealTimeText.text += "<null>";
            }
        }
    }

    public static void PauseEditor() // Pause-break button
    {
        Debug.Break();
    }
    
    public static void StartVideoScreenshoting()
    {
        ContinuousScreenshotingFrameNumber = 0;
        ContinuousScreenshotingKeyword = RandomString(5);
        Log("Continuous screenshotting started!");
    }
    
    public static void FinishVideoScreenshoting()
    {
        Application.OpenURL(ScreenshotsDirectory);
        Log("Continuous screenshotting ended!");
    }

    public static void MakeScreenshot()
    {
        ScreenCapture.CaptureScreenshot(ScreenshotsDirectory + "\\Screenshot__" + (DateTime.Now.ToString(CultureInfo.InvariantCulture)).Replace(':', '-').Replace('/', '_').Replace(' ', '_') + "__" + Time.time + ".png");
        Log("Screenshot captured");
        Application.OpenURL(ScreenshotsDirectory);
    }
    
    /// Updating some debug information
    public static void DebugUpdate()
    {
        // screenshotting all the time
        if (ContinuousScreenshoting)
        {
            ScreenCapture.CaptureScreenshot(ScreenshotsDirectory + "\\VideoScreenshot_" + ContinuousScreenshotingKeyword + "_" + ContinuousScreenshotingFrameNumber + "__" +
                                            (DateTime.Now.ToString(CultureInfo.InvariantCulture)).Replace(':', '-').Replace('/', '_').Replace(' ', '_') + ".png");
            ContinuousScreenshotingFrameNumber++;
        }

        OnUpdate(); // some debug actions that must be shown every moment
    }
    
    public static string RandomString(int length)
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return new string(Enumerable.Repeat(chars, length).Select(s => s[Random.Range(0, s.Length)]).ToArray());
    }
    
    public static void Log(params object[] text)
    {
        { foreach (object logText in text) Log(logText); }
    }

    public static void Log(object text, bool onlyConsole = false)
    {
        if (!onlyConsole) Debug.Log(text); // log only in editor
    }

    public static void LogError(object text, bool onlyConsole = false)
    {
        if (!onlyConsole) Debug.LogError(text); // log only in editor
    }

    public static void LogWarning(object text, bool onlyConsole = false)
    {
        if (!onlyConsole) Debug.LogWarning(text); // log only in editor
    }
}
