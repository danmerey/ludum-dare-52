﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Sounds : MonoBehaviour
    {
        const string SoundsFolderPrefix = "Sounds/";

        public AudioClip[] TestSound;
        
        private const float SOUNDS_VOLUME_MULTIPLIER = 1f;
        private const float MUSIC_VOLUME_MULTIPLIER = 1f;

        public static Sounds Library;
        
        static AudioSource SoundSourceCritical;
        static AudioSource SoundSourceImportant;
        static AudioSource SoundSourceStandard;
        static AudioSource SoundSourceMinor;
        static AudioSource SoundSourceMusic;

        public void Awake()
        {
            Library = this;

            Transform _transform = transform; 

            SoundSourceCritical = _transform.Find("Critical").GetComponent<AudioSource>();
            SoundSourceImportant = _transform.Find("Important").GetComponent<AudioSource>();
            SoundSourceStandard = _transform.Find("Standard").GetComponent<AudioSource>();
            SoundSourceMinor = _transform.Find("Minor").GetComponent<AudioSource>();
            SoundSourceMusic = _transform.Find("Music").GetComponent<AudioSource>();
        }

        public void Start()
        {
            // set temporary volume
            ChangeVolume();
        }

        public static void ChangeVolume()
        {
            float soundsVolume = (Main.SettingsSoundsMuted ? 0f : 1f) * SOUNDS_VOLUME_MULTIPLIER * Main.SettingsSoundsVolume;

            SoundSourceMinor.volume = soundsVolume;
            SoundSourceStandard.volume = soundsVolume;
            SoundSourceImportant.volume = soundsVolume;
            SoundSourceCritical.volume = soundsVolume;

            float musicVolume = (Main.SettingsMusicMuted ? 0f : 1f) * MUSIC_VOLUME_MULTIPLIER * Main.SettingsMusicVolume;
            SoundSourceMusic.volume = musicVolume;
        }

        public enum SoundPriority
        {
            Minor, // lowest (for frequent common sounds)
            Standard, // medium (for rare common sounds)
            Important, // high (for frequent important sounds)
            Critical, // highest (for rare important sounds)
        }

        public static void ResetSound() // reset sounds for new level or so
        {
        }

        public static void PlaySound(AudioClip clip, SoundPriority priority)
        {
            switch (priority)
            {
                case SoundPriority.Minor:
                {
                    SoundSourceMinor.PlayOneShot(clip);
                    break;
                }
                case SoundPriority.Standard:
                {
                    SoundSourceStandard.PlayOneShot(clip);
                    break;
                }
                case SoundPriority.Important:
                {
                    SoundSourceImportant.PlayOneShot(clip);
                    break;
                }
                case SoundPriority.Critical:
                {
                    SoundSourceCritical.PlayOneShot(clip);
                    break;
                }
            }
        }

        /// <summary>
        /// Play random sound from sounds array
        /// </summary>
        public static void PlaySound(AudioClip[] clipArray, SoundPriority priority)
        {
            if (clipArray.Length > 0) PlaySound(clipArray[Random.Range(0, clipArray.Length)], priority);
            else DebugGame.Log("No sounds in clip array. Better record some :)");
        }
    }
}
