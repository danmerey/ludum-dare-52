using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapBorder : MonoBehaviour
{
    private const float RETURN_SHIP_BACK_FORCE = 5.25f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        other.attachedRigidbody.AddForce(-other.transform.position.normalized * RETURN_SHIP_BACK_FORCE);
        Main.WindAngleDegrees = AngleMath.AngleBetween2dPointsDeg(other.transform.position, Vector2.zero);
    }
}
