using UnityEngine;

public class AngleMath : MonoBehaviour
{
    public static float NormalizeAngleDegrees(float angle)
    {
        float newAngle = angle;
        while (newAngle >= 360) newAngle -= 360;
        while (newAngle < 0) newAngle += 360;
        return newAngle;
    }

    public static float AngleBetween2dPointsRad(Vector2 fromPoint, Vector2 toPoint)
    {
        //return Mathf.Atan2(fromPoint.y - fromPoint.y, toPoint.x - toPoint.x);
        return Mathf.Atan2(toPoint.y - fromPoint.y, toPoint.x - fromPoint.x);
    }
    
    public static float AngleBetween2dPointsDeg(Vector2 fromPoint, Vector2 toPoint)
    {
        return AngleBetween2dPointsRad(fromPoint, toPoint) * Mathf.Rad2Deg;
    }
}
