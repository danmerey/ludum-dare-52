using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Fish : MonoBehaviour
{
    public enum FishTypes
    {
        FishyFish = 0,
        CoolCod = 1,
        SnappySnapper = 2,
        ShyShark = 3,
        SageSalmon = 4,
        CageyCatfish = 5,
        GiantGoldfish = 6,
    }

    private const float MOVE_HOOKEDFISH_TOWARDS_HOOK_SPEED = 0.01f;
    private const float SPEED_OF_RETURNING_HOOKED_FISH_FROM_THE_DEEP = 10f; // healing speed
    
    public FishTypes FishType = FishTypes.FishyFish;
    public float MoveSpeed = 1f;
    public float RotationSpeed = 1f;
    
    private const float FISH_SPEED_MULITPLIER = 0.03f;
    private const float FISH_ROTATION_MULITPLIER = 0.00001f;
    
    private const float MAX_LIFE_RANDOM_MIN = 30f;
    private const float MAX_LIFE_RANDOM_MAX = 120f;
    
    private float MaxLife;
    private float Life;

    private Transform Hook;

    private bool Hooked;

    private Transform _transform;
    private Rigidbody2D _rigidbody2D;
    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _transform = GetComponent<Transform>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        SpawnFish();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!Hooked)
        {
            Life -= Time.deltaTime;
            if (Life <= 0)
            {
                Destroy(gameObject);
            }
            else
            {
                ChangeSpriteTransparency();
            }

            float faceAngleRad = _transform.eulerAngles.z * Mathf.Deg2Rad;
            _rigidbody2D.AddForce(new Vector2(Mathf.Cos(faceAngleRad), Mathf.Sin(faceAngleRad)) * (MoveSpeed * FISH_SPEED_MULITPLIER), ForceMode2D.Force);
            
            float randomAngleRad = faceAngleRad + Random.Range(-Mathf.PI, Mathf.PI);
            _rigidbody2D.AddTorque(randomAngleRad * RotationSpeed * FISH_ROTATION_MULITPLIER, ForceMode2D.Force);
            
            FishAI();
        }
        else // hooked
        {
            Life = Mathf.MoveTowards(Life, MaxLife / 2f, SPEED_OF_RETURNING_HOOKED_FISH_FROM_THE_DEEP * Time.deltaTime); // returning from the deep
            ChangeSpriteTransparency();

            _transform.localPosition = Vector3.MoveTowards(_transform.localPosition, Vector3.zero, MOVE_HOOKEDFISH_TOWARDS_HOOK_SPEED * Time.deltaTime);
        }
    }

    void SpawnFish()
    {
        MaxLife = Random.Range(MAX_LIFE_RANDOM_MIN, MAX_LIFE_RANDOM_MAX);
        Life = MaxLife;
        
        ChangeSpriteTransparency();
    }

    void ChangeSpriteTransparency()
    {
        Color color = _spriteRenderer.color;
        color.a = Mathf.Pow(Mathf.Cos(((2 * Life / MaxLife) - 1f) * Mathf.PI * 0.5f), 0.5f);
        _spriteRenderer.color = color;
    }

    public void SetRandomLife() 
    {
        Life = Random.Range(0, MaxLife);
    }

    private void FishAI()
    {
        if (FishType == FishTypes.ShyShark) // shy shark is very shy and avoid player all the time
        {
            if (Main.PlayerShip != null)
            {
                const float SPEED_OF_TURNING = 30f;
                float angleDeg = AngleMath.AngleBetween2dPointsDeg(_transform.position, Main.PlayerShip._transform.position);
                _transform.eulerAngles = new Vector3(0, 0, Mathf.MoveTowardsAngle(_transform.eulerAngles.z, angleDeg + 180, SPEED_OF_TURNING * Time.deltaTime));
            }
        }
        else if (FishType == FishTypes.SageSalmon) // sage salmon is pretty smart and always swim against the wind
        {
            if (Main.PlayerShip != null)
            {
                const float SPEED_OF_TURNING = 30f;
                _transform.eulerAngles = new Vector3(0, 0, Mathf.MoveTowardsAngle(_transform.eulerAngles.z, Main.WindAngleDegrees + 180, SPEED_OF_TURNING * Time.deltaTime));
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        _transform.SetParent(col.transform.Find("CaughtFish"));
        _rigidbody2D.simulated = false;
        Hooked = true;
    }
}
