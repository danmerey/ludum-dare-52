﻿using System;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    public static Camera MainGameCamera;
    private static Transform _transformZoom;
    
    private const float CAMERA_ZOOM_SPEED_MULTIPLIER = 0.225f;
    private const float CAMERA_ZOOM_FARTHEST = -10; // also multiplies on fog size after
    private const float CAMERA_ZOOM_CLOSEST = -3;
    
    public static Transform _cameraAimTransform;
    private Transform _cameraFocusTransform;


    private float lastCameraAngle = 0f;

    private void Awake()
    {
        _cameraFocusTransform = transform; // where the camera is now
        _transformZoom =  _cameraFocusTransform.Find("Zoom");
        MainGameCamera = _transformZoom.Find("Camera").GetComponent<Camera>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Zones.UpdateCameraFocusZone(_transformCamera.position);
        _cameraAimTransform = GameObject.Find("CameraAim").transform; // where camera wants to be soon
    }

    // Update is called once per frame
    void Update()
    {
        // zoom camera
        {
            float delta = Main.CameraZoomSpeed * CAMERA_ZOOM_SPEED_MULTIPLIER;
            _transformZoom.localPosition = new Vector3(_transformZoom.localPosition.x, _transformZoom.localPosition.y, Mathf.Clamp(_transformZoom.localPosition.z + delta,
                CAMERA_ZOOM_FARTHEST * Upgrades.CurrentVisibilityRadius * Upgrades.FOG_SIZE_MULTIPLIER,
                CAMERA_ZOOM_CLOSEST));
        }
        
        // move camera to camera aim
        {
            _cameraFocusTransform.position = _cameraAimTransform.position;
        }
    }
    public static float Zoom()
    {
        return _transformZoom.localPosition.z;
    }
}
