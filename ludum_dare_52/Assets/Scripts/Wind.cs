using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Wind : MonoBehaviour
{
    public const int WIND_PARTICLES_NUMBER = 30;
    private const float WIND_SPEED_MULTIPLIER = 2.5f;
    private const int WIND_MIN_SPAWN_DISTANCE_FROM_SHIP = 3;
    private const int WIND_MAX_SPAWN_DISTANCE_FROM_SHIP = 7;
    private const float MAX_LIFE_RANDOM_MIN = 1f;
    private const float MAX_LIFE_RANDOM_MAX = 3f;
    
    private float MaxLife;
    private float Life;

    private Transform _transform;
    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _transform = GetComponent<Transform>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        SpawnParticle();
    }

    // Update is called once per frame
    void Update()
    {
        // move particle
        float windAngleRad = (Main.WindAngleDegrees) * Mathf.Deg2Rad;
        _transform.position += new Vector3(Mathf.Cos(windAngleRad), Mathf.Sin(windAngleRad), 0) * (Main.WindStrength * WIND_SPEED_MULTIPLIER * Time.deltaTime);

        Life -= Time.deltaTime;
        if (Life <= 0)
        {
            SpawnParticle(); // spawn it again
        }
        else
        {
            ChangeSpriteTransparency();
            ChangeRotation();
        }
    }

    void SpawnParticle()
    {
        MaxLife = Random.Range(MAX_LIFE_RANDOM_MIN, MAX_LIFE_RANDOM_MAX);
        Life = MaxLife;

        float randomRadius = Random.Range(WIND_MIN_SPAWN_DISTANCE_FROM_SHIP, WIND_MAX_SPAWN_DISTANCE_FROM_SHIP);
        float randomAngleRad = Random.Range(0, Mathf.PI * 2);

        _transform.position = Main.PlayerShip._transform.position + new Vector3(Mathf.Cos(randomAngleRad), Mathf.Sin(randomAngleRad), 0) * randomRadius;
        
        ChangeSpriteTransparency();
        ChangeRotation();
    }

    void ChangeSpriteTransparency()
    {
        Color color = _spriteRenderer.color;
        color.a = Mathf.Cos(((2 * Life / MaxLife) - 1f) * Mathf.PI * 0.5f);
        _spriteRenderer.color = color;
    }

    void ChangeRotation()
    {
        _transform.eulerAngles = new Vector3(0, 0, Main.WindAngleDegrees);
    }
}
